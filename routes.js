var express = require('express');
var router = express.Router();
var Employee = require('./model');
router.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});




router.get('/', (req, res) => {
    Employee.getEmployees((err, employees) => {
        if (err) throw err;
        res.json(employees);
    });
    // res.send("Hello from the router...");
});

router.post('/', (req, res) => {
    var newEmployee = {
        name: req.body.name,
        position: req.body.position,
        department: req.body.department,
        start_time: req.body.start_time,
        end_time: req.body.end_time
    }
    Employee.addEmployees(newEmployee, (err, employees) => {
        if (err) throw err;
        res.json(employees);
    });

});

router.put('/:_id', (req, res) => {
    var update = {
        name: req.body.name,
        position: req.body.position,
        department: req.body.department,
        start_time: req.body.start_time,
        end_time: req.body.end_time
    }

    Employee.updateEmployees(req.params._id, update, (err, employees) => {
        if (err) throw err;
        res.json(employees);
    }
    )
});


router.get('/:_id', (req, res) => {
    Employee.getEmployee(req.params._id, (err, employees) => {
        if (err) throw err;
        res.json(employees);
    });
});

router.delete('/:_id', (req, res) => {
    Employee.deleteEmployees(req.params._id, (err, employees) => {
        if (err) throw err;
        res.json(employees);
    });
});

module.exports = router;