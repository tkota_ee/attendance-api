var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var config = "mongodb://localhost/employees";
mongoose.connect(config)
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...', err));

var app = express();
var port = 3000;

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

app.get('/', (req, res) => {
    res.send("Hello from kota...");
});

var router = require('./routes');
// middleware
app.use(bodyParser.urlencoded({
    exstended: true
}));
app.use(bodyParser.json());

app.use('/api/employees', router);

app.listen(port, () => {
    console.log("server is running on port " + port);
});