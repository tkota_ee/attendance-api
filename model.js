var mongoose = require('mongoose');
var empSchema = new mongoose.Schema({
    name: String,
    position: String,
    department: String,
    start_time: Number,
    end_time: Number,
    createdDate: {
        type: Date,
        default: Date.now
      }
});


var Employee = module.exports = mongoose.model('Employee', empSchema);


module.exports.getEmployees = (callback) => {
    // console.log('hello kota');
    Employee.find(callback);
};

module.exports.addEmployees = (newEmployee, callback) => {
    Employee.create(newEmployee, callback);
};

module.exports.updateEmployees = (id, newEmployee, callback) => {
    Employee.findByIdAndUpdate(id, newEmployee, callback);
};

module.exports.deleteEmployees = (id, callback) => {
    Employee.findByIdAndRemove(id, callback);
};
module.exports.getEmployee = (id, callback) => {
    Employee.findById(id, callback);
};